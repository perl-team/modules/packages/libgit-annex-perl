libgit-annex-perl (0.008-3) UNRELEASED; urgency=medium

  * Drop tzdata build-dep again.
    See <https://bugs.debian.org/cgi-bin/1099359#14>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 07 Mar 2025 19:57:12 +0800

libgit-annex-perl (0.008-2) unstable; urgency=medium

  * Add build-dep on tzdata (Closes: #1099316).
    Thanks to Jochen Sprickerhof for the report.

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 04 Mar 2025 19:31:15 +0800

libgit-annex-perl (0.008-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 23 Dec 2022 23:08:58 -0700

libgit-annex-perl (0.007-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends-Indep: Drop versioned constraint on perl.
    + libgit-annex-perl: Drop versioned constraint on perl in Depends.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 06 Dec 2022 15:10:49 +0000

libgit-annex-perl (0.007-1) unstable; urgency=medium

  [ Sean Whitton ]
  * New upstream release.
  * Add compatibility symlink annex-review-unused -> git-annex-reviewunused.

  [ Debian Janitor ]
  * Set field Upstream-Name in debian/copyright.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 28 Feb 2021 13:30:14 -0700

libgit-annex-perl (0.006-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 24 Mar 2020 11:12:23 -0700

libgit-annex-perl (0.005-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 23 Mar 2020 12:03:32 -0700

libgit-annex-perl (0.004-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 18 Mar 2020 17:30:48 -0700

libgit-annex-perl (0.003-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 11 Mar 2020 14:43:32 -0700

libgit-annex-perl (0.002-2) unstable; urgency=medium

  [ gregor herrmann ]
  * Add debian/tests/pkg-perl/use-name and switch to standard pkg-perl
    autotests.

  [ Sean Whitton ]
  * Tighten git-annex build-dep to require 7.20191009.
    For the sake of attempts to build this package on buster.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 28 Feb 2020 16:05:33 -0700

libgit-annex-perl (0.002-1) unstable; urgency=medium

  * New upstream release.
  * Use dh-dist-zilla.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 24 Feb 2020 07:40:31 -0700

libgit-annex-perl (0.001-1) unstable; urgency=medium

  * Initial upload to Debian.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 12 Feb 2020 13:03:44 -0700
